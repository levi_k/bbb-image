#!/bin/bash
set -x

groups="adm,dialout"

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true LC_ALL=C LANGUAGE=C LANG=C
ln -s /usr/lib/insserv/insserv /sbin/insserv
/var/lib/dpkg/info/dash.preinst install
dpkg --configure -a ; rm -r /var/run/* ; dpkg --configure -a
dpkg --install /tmp/gadget-upstart_1.4cross_all.deb /tmp/linux-3.13-rc0.deb
useradd -mU ubuntu -s /bin/bash --groups $groups
echo ubuntu:ubuntu | chpasswd
mkdir -p "/home/ubuntu/.series 1/uploads"
chown -R ubuntu:ubuntu /home/ubuntu
mv /sbin/start.bak /sbin/start
rm /tmp/gadget-upstart_1.4cross_all.deb /tmp/linux-3.13-rc0.deb
exit
